package gnukhata.controllers.reportmodels;

public class transaction {
	private String transactionDate;
	private String particulars;
	private String voucherNo;
	private String dr;
	private String cr;
	private String narration;
	private String voucherCode;
	public transaction(String transactionDate, String particulars,
			String voucherNo, String dr, String cr, String narration, String voucherCode) {
		super();
		this.transactionDate = transactionDate;
		this.particulars = particulars;
		this.voucherNo = voucherNo;
		this.dr = dr;
		this.cr = cr;
		this.narration = narration;
		this.voucherCode = voucherCode;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public String getParticulars() {
		return particulars;
	}
	public String getVoucherNo() {
		return voucherNo;
	}
	public String getDr() {
		return dr;
	}
	public String getCr() {
		return cr;
	}
	public String getNarration() {
		return narration;
	}
	
	/**
	 * @return the voucherCode
	 */
	public int getVoucherCode() {
		return Integer.parseInt(this.voucherCode);
	}
	@Override
	public String toString() {
		return "transaction [voucherNo=" + voucherNo + "]";
	}


}
